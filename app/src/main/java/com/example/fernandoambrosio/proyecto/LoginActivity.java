package com.example.fernandoambrosio.proyecto;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {

    private EditText et1, et2;
    private Button bt1, bt2;
    private Cursor fila;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

       DbHelper helper = new DbHelper(this);
        SQLiteDatabase db = helper.getWritableDatabase();

        et1= (EditText) findViewById(R.id.et1);
        et2= (EditText) findViewById(R.id.et2);
        bt1= (Button) findViewById(R.id.bt1);
        bt2= (Button) findViewById(R.id.bt2);
    }

    

    public void ingresar(View v){
        DbHelper admin=new DbHelper(this);
        SQLiteDatabase db=admin.getWritableDatabase();
        String usuario=et1.getText().toString();
        String contrasena=et2.getText().toString();


        fila=db.rawQuery("select usuario,contrasena from usuarios where usuario='"+usuario+"' and contrasena='"+contrasena+"'",null);



        if (fila.moveToFirst()){
            String usua=fila.getString(0);
            String pass=fila.getString(1);
            if (usuario.equals(usua)&&contrasena.equals(pass)){
                Intent ven=new Intent(this,EstudainteActivity.class);
                startActivity(ven);
                et1.setText("");
                et2.setText("");
            }

        }

        else if  (usuario.endsWith("administrador") && contrasena.equals("admin")) {
            Intent e = new Intent(this, AdministradorActivity.class);
            startActivity(e);
        }

    }



    public void vregistro(View view){
        Intent i = new Intent(this, RegistroActivity.class);
        startActivity(i);
    }
}
