package com.example.fernandoambrosio.proyecto;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class RegistroActivity extends AppCompatActivity {

    private EditText txt3, txt4, txt5;
    private Button bt3, bt4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        txt5 = (EditText) findViewById(R.id.txt5);
        txt3= (EditText) findViewById(R.id.txt3);
        txt4= (EditText) findViewById(R.id.txt4);
        bt3= (Button) findViewById(R.id.bt3);
        bt4= (Button) findViewById(R.id.bt4);

    }


    public void registrar(View view){

        DbHelper admin=new DbHelper(this);
        SQLiteDatabase db=admin.getWritableDatabase();
        String codigo=txt5.getText().toString();
        String usuario=txt3.getText().toString();
        String contraseña=txt4.getText().toString();

        ContentValues values=new ContentValues();
        values.put("codigo",codigo);
        values.put("usuario",usuario);
        values.put("contrasena",contraseña);

        db.insert("usuarios",null,values);
        db.close();

        Intent ven=new Intent(this,LoginActivity.class);
        startActivity(ven);

    }

    public void regresar(View view){
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
    }
}
